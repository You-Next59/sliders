json.extract! slide, :id, :name, :desc, :author, :created_at, :updated_at
json.url slide_url(slide, format: :json)